'use strict';
module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        //Watch items
        watch: {
            express: {
                files:  [ 'server/**/*.js'],
                tasks:  [ 'express:dev', 'express:cms', 'wait'],
                options: {
                    spawn: false, // for grunt-contrib-watch v0.5.0+, "nospawn: true" for lower versions. Without this option specified express won't be reloaded
                    nospawn: true
                }
            },
            app: {
                files: ['app/*.html',
                        'app/scripts/**/*.js',
                        'app/styles/*.css',
                        'app/templates/**/*.hbs',
                        'cms/*.html',
                        'cms/scripts/**/*.js',
                        'cms/styles/*.css',
                        'cms/templates/**/*.hbs'],
                options:{
                    livereload: true
                }
            },
            less:{
                files: ['app/styles/*.less'],
                tasks: ['less:dev']
            },
            less_cms:{
                files: ['cms/styles/*.less'],
                tasks: ['less:cms']
            }
        },
        // used after reloading the express server
        wait: {
            options: {
                delay: 1000
            },
            pause: {      
                options: {
                    before : function(options) {
                        console.log('pausing %dms', options.delay);
                    },
                    after : function() {
                        console.log('pause end');
                    }
                }
            }          
        },        

        // express app
        express: {
            dev: {
                options: {
                    script: 'server/app.js',
                    node_env: 'development',
                    port: '9000'
                }
            },
             cms: {
                options: {
                    script: 'server/app.js',
                    node_env: 'cms',
                    port: '9000'
                }
            },
            prod: {
                options: {
                    script: 'server/app.js',
                    node_env: 'production'
                }
            }
        },
        
        // open app and test page
        open: {
            dev: {
                path: 'http://localhost:<%= express.dev.options.port %>'
            },
            cms:{
                path: 'http://localhost:<%= express.cms.options.port %>'
            }
        },

        // Task for Less compilation
        less:{
            dev: {
                options: {
                   compress: false,  // no minification in dev
                   },
                files: {
                   "./app/styles/main.css":"./app/styles/base.less"
                   }
            },
            cms:{
                options: {
                   compress: false,  // no minification in dev
                   },
                files: {
                   "./cms/styles/main.css":"./cms/styles/base.less"
                }
            },
            production: {
                options: {
                   cleancss: true, // minify css
                   compress: true,
                },
                files: {
                   "./dist/styles/main.css": "./app/styles/base.less"
                }
            }
        },

        //handlebars
        handlebars: {
            compile: {
                options: {
                    namespace: 'JST',
                    amd: true
                },
                files: {
                    '.tmp/scripts/templates.js': ['app/templates/**/*.hbs', 'app/templates/*.hbs']
                }
            }
        },

        //requirejs
        requirejs: {
            dist: {
                options: {
                    almond: true,
                    baseUrl: 'app/scripts',
                    mainConfigFile: 'app/scripts/init.js',
                    include: 'init',
                    name: '../../app/bower/almond/almond',
                    out: 'dist/scripts/main.js',
                    paths: {
                        'templates': '../../.tmp/scripts/templates'
                    },
                    replaceRequireScript: [
                        {
                            files:['dist/index.html'],
                            module: 'init',
                            modulePath: 'scripts/main'
                        }
                    ],
                    optimize: 'uglify2',
                    uglify2:{
                        mangle:{
                            except: ["$super"]
                        }
                    },
                    preserveLicenseComments: false,
                    useStrict: true,
                    wrap: true,
                    pragmasOnSave: {
                        excludeHbsParser : true,
                        excludeHbs: true,
                        excludeAfterBuild: true
                    }
                }
            }
        },

        //minify html
        htmlmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: '*.html',
                    dest: 'dist'
                }],
                options:{
                    removeComments: true,
                    collapseWhitespace: true
                }
            }
        },
        clean: {
            dist: ['.tmp', 'dist/*'],
            tmp: '.tmp'
        },
        //Clean up HTML, strip livreload
        processhtml:{
            dist: {
              files: {
                'dist/index.html': ['app/index.html']
              }
            }
        },
    });

    grunt.registerTask('createDefaultTemplate', function () {
        grunt.file.write('.tmp/scripts/templates.js', 'this.JST = this.JST || {};');
    });

    grunt.registerTask('default', function (target) {
        grunt.task.run([
            'less:dev',
            'express:dev',
            'open:dev',
            'watch'
        ]);
    });

    grunt.registerTask('cms', function (target) {
        grunt.task.run([
            'less:cms',
            'express:cms',
            'open:cms',
            'watch'
        ]);
    });    

    grunt.registerTask('build', function (target) {
        grunt.task.run([
            'clean:dist',
            'createDefaultTemplate',
            'handlebars',
            'less:production', 
            'processhtml:dist',       
            'htmlmin',
            'requirejs',
            'clean:tmp'
        ]);
    });
};

