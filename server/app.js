'use strict';

var express = require('express');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');

var api = require('./components/api');
var cms = require('./components/cms');


var app = express();


if(process.env.NODE_ENV == 'development'){

	app.set('port',9000);

	app.use(bodyParser.json({limit: '50mb',parameterLimit:100000}));
	app.use(bodyParser.urlencoded({limit: '50mb',parameterLimit:100000, extended: true}));
	
	app.use(function(req,res,next){
		console.log('%s %s', req.method, req.url);
		next();
	});
	
	app.get('/', function (req, res) {
		res.sendFile( path.join( __dirname, '../app/index.html' ) );
	});
	
	app.use(express.static( path.join( __dirname, '../app') ));
}

if(process.env.NODE_ENV == 'cms'){
	app.set('port',9000);

	app.use(bodyParser.json({limit: '50mb',parameterLimit:100000}));
	app.use(bodyParser.urlencoded({limit: '50mb',parameterLimit:100000, extended: true}));
	
	app.use(function(req,res,next){
		console.log('%s %s', req.method, req.url);
		next();
	});

	app.get('/',function (req,res) {
		res.sendFile( path.join( __dirname, '../cms/index.html' ) );		
	});

	app.use(express.static( path.join( __dirname, '../cms') ));	
}

if(process.env.NODE_ENV == 'production'){
	app.set('port',5121);

	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());

	app.use(express.static( path.join( __dirname, '../dist') ));
}


//API routes
app.get('/api/v1/welcome', api.welcome);
app.get('/api/v1/GetGasPriceByProvince', api.GetGasPriceByProvince);
app.get('/api/v1/CheckBlacklist', api.CheckBlacklist);

app.post('/api/v1/CMS/upsertDB_AMP',cms.upsert_AMAP);
app.get('/api/v1/CMS/createBlackListIndex',cms.createIndex_AMAP);


http.createServer(app).listen(app.get('port'), function () {
 	console.log( 'Express server listening on port %d in %s mode', app.get('port'), app.settings.env );
}); 