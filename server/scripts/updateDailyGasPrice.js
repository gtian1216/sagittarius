"use strict"

var Rukbat = require('mongodb').MongoClient;
var request = require('request');
var async = require('async');
var utils = require('../utilities');
var conf  = require('../config');
var China = require('./china-province-and-city-data.js');
var _ = require('underscore');

var provinces = [];
_.each( _.pluck(China.provinces,'text'),function(province){
	province = utils.convert_province_name(province);
	provinces.push(province);
});

var gas_api = 'http://api.46644.com/oil';
var gas_data = {appkey : 'd032bc1c45093f812d053aee7c3b642a'};
/*
http://www.46644.com/
http://api.46644.com/oil/?province=广东&appkey=d032bc1c45093f812d053aee7c3b642a
您申请的APPKEY信息如下：
APPID：nt09150558852
APPKEY：d032bc1c45093f812d053aee7c3b642a
接口内容：
频率：200次/日
*/

Rukbat.connect(conf.dbhost, function (err, db) {
  	if (err) {
    	utils.handle_DB_error(err,db);
  	} else {
	    console.log('This is Rukbat, the alpha sagittarii, welcome home Captain');
	    var gas = db.collection('GAS_DAILY_PRICE');
	    var data = [];
	    

	    var gasInsert = function(data){
			async.each(data,function(gas_data,callback){
				gas.update({'province':gas_data.province},gas_data,{upsert:true},function(err){
					callback();
				});
			},function(err){
				if(err){
					utils.handle_DB_error(err,db);
				}else{
					ensureIndex();
				}
			})
	    }

	    var ensureIndex = function(){
	    	gas.indexInformation(function(err,idx){
	    		if(err){ utils.handle_DB_error(err,db);}

	    		if(idx.province == undefined){
	    			gas.createIndex({'province':1},{name:'province'},function(err,result){
	    				if(err){ utils.handle_DB_error(err,db);}
	    				console.log('Operation completed with Indexing. See you again Captain');
	    				db.close();
	    			});
	    		}else{
	    			console.log('Operation completed without Indexing. See you again Captain');
	    			db.close();
	    		}
	    	});
	    }


	    async.each(provinces,function(province,callback){
	    	gas_data.province = province;
			request({url:gas_api,qs:gas_data,json:true},function(error, response, res){
		    	if (!error && response.statusCode === 200) {
		    		delete res.id;
		        	delete res.addtime;
		        	if(res != ''){
		        		data.push(res);
		        	}
		    	}
		    	callback();
			});			
	    },function(err){
		    if(err) {
    			utils.handle_DB_error(err,db);
		    } else {
		    	gasInsert(data);
		    }  	
	    });

	}
});
