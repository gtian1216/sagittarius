'use strict';

var _ = require('underscore');
var conf  = require('../config.js');
var utils = require('../utilities.js');
var Rukbat = require('mongodb').MongoClient;

/***********
路径规划API (http://lbs.amap.com/api/webservice/reference/direction/)

http://restapi.amap.com/v3/direction/driving?params
http://restapi.amap.com/v3/direction/driving?origin=116.481028,39.989643&destination=116.465302,40.004717&extensions=all&output=xml&key=<用户的key>

@params
origin:
destination
extensions=all
output=json
key=ea19ca03c4357c693677cbc3e9d33f6d
************/
exports.upsert_AMAP = function(req,res){
	var road = req.body;
	Rukbat.connect(conf.dbhost, function (err, db) {
	  	if (err) {
	    	utils.handle_DB_error(err,db);
	  	} else {		   
			var RD = db.collection('ROAD_BLACKLIST');
			road.start_end = {start_loc:road.start_location,end_loc:road.end_location};
			RD.update({'path':road.path},road,{upsert:true},function(err,result){
				if (err) {utils.handle_DB_error(err,db);}
				console.log(result);		
				res.send(result);
				db.close();
			});
		}
	});
}

exports.createIndex_AMAP = function(req,res){
	var blacklistIdx1 = false;
	var blacklistIdx2 = false;
	Rukbat.connect(conf.dbhost, function (err, db) {
	  	if (err) {
	    	utils.handle_DB_error(err,db);
	  	} else {		   
			var RD = db.collection('ROAD_BLACKLIST');
			RD.createIndex({'path':'2dsphere','weight':1},{name:'blacklistIdx1'},function(err,result){
	    		if(err){ utils.handle_DB_error(err,db);}
	    		blacklistIdx1 = true;
	    		checkResult(db);
	    	});
	    	RD.createIndex({'start_end':1,'weight':1},{name:'blacklistIdx2'},function(err,result){
	    		if(err){ utils.handle_DB_error(err,db);}
	    		blacklistIdx2 = true;
	    		checkResult(db);
	    	});
		}
	});	
	function checkResult(db){
		if(blacklistIdx1 && blacklistIdx2){
			db.close();
			res.send('Successed');
		}
	}
}