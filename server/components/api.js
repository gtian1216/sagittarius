"use strict"

var _ = require('underscore');
var conf  = require('../config.js');
var utils = require('../utilities.js');
var Rukbat = require('mongodb').MongoClient;

exports.welcome = function(req,res){
	var message = '';
	
	message+= '<b>Greeting Traveller, Welcome to Sagittarius </b><br>';
	message+= 'Sagittarius (♐) (Greek: Τοξότης Toxotes, Latin: Sagittarius) is the ninth astrological sign, which is associated with the constellation Sagittarius. <br>';
	message+= 'Under the tropical zodiac, sun transits this sign between November 23 and December 21.<br>';
	message+= 'The symbol of the archer is based on the centaur Chiron, who mentored Achilles in archery.';

	res.send(message);
}

/**
 * Check if the road is blocked / blacklisted against database
 *
 * @param {req obj} req req.query.road: name of the road to check
 * @param {res obj} res http response object
 * @return {mongo record} return false: road is not blocked/blacklisted, result: road blocked/blacklisted
 */
exports.CheckBlacklist = function(req,res){
	var start = req.query.path.start_location.map(function(i){return parseFloat(i)});
	var end = req.query.path.end_location.map(function(i){return parseFloat(i)});
	var weight = req.query.weight;
	Rukbat.connect(conf.dbhost, function(err,db){
		if(err) utils.handle_DB_error(err,db,res);
		var RD = db.collection('ROAD_BLACKLIST');

		var query = {'start_end':{'start_loc':start,'end_loc':end},weight:{'$lte':weight}};

		RD.findOne(query,function(err,result){
			
			if(result == null ){
				res.send(false)
			}else{
				res.send(result);
			}
		});
	});
}


/**
 * Get the daily price by province
 *
 * @param {req obj} req req.query.province: name of the province to check
 * @param {res obj} res http response object
 * @return {boolean} return the daily gas price for given province
 */
exports.GetGasPriceByProvince = function(req,res){
	Rukbat.connect(conf.dbhost,function(err,db){
		if(err) utils.handle_DB_error(err,db,res);
		
		var gas = db.collection('GAS_DAILY_PRICE');
		var province = utils.convert_province_name(req.query.province);
		
		var query = {
			'province' : province
		};
		
		gas.findOne(query,function(err,price){
			if(err) utils.handle_DB_error(err,db,res);
			res.send(price);
			db.close();
		});
	});
}