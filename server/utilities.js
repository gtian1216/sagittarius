"use strict"

/**
 * Handles database error
 *
 * @param {string} err database error message
 * @param {db} db database connection object
 * @param {http response} res http response object (optional)
 */
exports.handle_DB_error = function(err,db,res){
	console.log(err);
	if(res) res.send(false);
	db.close();
}

/**
 * Convert Porvince name to 2 Chinese character format
 *
 * @param {string} province province name in full format
 * @return {string} province province name in 2 Chinese characters format
 */
exports.convert_province_name = function(province){
	return province.replace(new RegExp("(省|市|自治区|壮族自治区|回族自治区|维吾尔自治区)","gi"),"")
}