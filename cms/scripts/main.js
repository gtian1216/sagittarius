require([
  'backbone',
  'app',
  'region',
  'modules/menu',
  'modules/amap',
],
function (Backbone, app, region ) {
  'use strict';	
  app.start();
});
