
define([
	'backbone',
	'communicator',
	'views/items/menu'
],

function( Backbone, Communicator, menu) {
	'use strict';

	var App = new Backbone.Marionette.Application();

	App.addInitializer( function () {
		Communicator.reqres.request('region:add', 'tab', '#tab');
		Communicator.reqres.request('region:add', 'content', '#content');
		App.menu_r = Communicator.reqres.request('region:get', 'tab');
		App.content_r = Communicator.reqres.request('region:get','content'); 
		App.menu_r.show(new menu());
		App.Menu.display(App.content_r);
	});
	
	return App;
});
