define( ['backbone','communicator','hbs!tmpl/layouts/menu','models/twopoints'],
function( Backbone, Communicator,menu_l, point) {
    'use strict';
	return Backbone.Marionette.LayoutView.extend ({
		initialize: function() {
            Communicator.reqres.setHandler('menu:pauseSuggestion',this.pauseSuggestion,this);
            Communicator.reqres.setHandler('menu:currentFocusInput',this.currentFocusInput,this);
            Communicator.command.setHandler('menu:setInput',this.setInput,this);
            Communicator.mediator.on('map:doneOp',this.hideComputing,this);

            this.model = new point({start_loc:{},end_loc:{},start_lock:false,end_lock:false});
		},
        events:{
            'input .menu-address':'autoComplete',
            'propertychange .menu-address':'autoComplete',
            'focus .menu-address':'moveSuggestion',
            'focusout .menu-address':'clearSuggestion',
            'click #navigate':'navigation',
        },
        template: menu_l,
        regions:{
            menu:'#routes-menu',
            route:'#routes-list',
            mapContainer:'#mapContainer',
            suggestion:'#menu-suggestion'
        },
        autoComplete:function(ev){
            Communicator.reqres.request("map:autoCompleteSuggestion",$(ev.target).val(),function(data){
                Communicator.mediator.trigger('map:suggestion',data);
            });
        },
        //Move suggestion window and set current focus input field, and unlock the field for ops    
        moveSuggestion:function(ev){
            this.focusedInput = $(ev.target).attr('id');
            if(this.focusedInput == 'start')
                this.model.set({start_lock:false});
            else{
                this.model.set({end_lock:false});
            } 
            this.$el.find('#menu-suggestion').insertAfter($(ev.target).closest('div'));
        },
        clearSuggestion:function(){
            if(!this.$el.find('#menu-suggestion').is(":hover")){
                Communicator.mediator.trigger('map:suggestion',false);
            }
        },
        pauseSuggestion:function(){
             if(this.$el.find('#'+this.focusedInput).val().length <= 0  || this.model.get(this.focusedInput+'_lock')){
                return true;
             }else{
                return false
             }
        },
        currentFocusInput:function(){
            return this.focusedInput;
        },
        //Set input field to selected item from suggestion and lock the field
        setInput:function(data,point){
            this.$el.find('#'+point).val(data.name+' '+data.district);
            this.$el.find('#'+point).data(data);
            if(point == 'start'){
                this.model.set('start_loc',data.location);
                this.model.set('start_lock',true);
                Communicator.command.execute('map:getAddressByLngLat',[data.location.lng,data.location.lat]);
            }else{
                this.model.set('end_loc',data.location);
                this.model.set('end_lock',true);               
            }
        },
        navigation:function(){
            if(this.model.get('start_lock') && this.model.get('end_lock')){
                var s = this.model.get('start_loc');
                var e = this.model.get('end_loc');
                var w = this.$el.find('#vehicle-weight').val();
                this.$el.find('#menu-computing').show();
                Communicator.command.execute('map:navigation',{lng:s.lng,lat:s.lat},{lng:e.lng,lat:e.lat},w);
            }
        },
        hideComputing:function(){
            this.$el.find('#menu-computing').hide();
        },
        onRender: function() {
        }
    });
});