define([
	'backbone',
	'communicator',
	'hbs!tmpl/items/route'
],
function( Backbone, Communicator, route  ) {
    'use strict';
	return Backbone.Marionette.ItemView.extend({
		initialize: function() {
		},
		tagName:'li',
		className:'list-group-item info-route-list-item',
	  	template: function(step) {
	      return route(step);
	    },
  		ui: {},
		events: {
			'mouseover .info-route-highlight':'drawTraceLine',
			'mouseout  .info-route-highlight':'clearTraceLine',
			'click .ban-road':'banRoad'
		},
		banRoad:function(){
			var url = '/api/v1/CMS/upsertDB_AMP';
			var json = this.model.toJSON();
			var data = {};
			data.road = json.road;
			data.path = {};
			data.path.type = 'LineString';
			data.path.coordinates = [];
			for(var i in json.path){
				data.path.coordinates.push([json.path[i].lng,json.path[i].lat]);
			}

			data.start_location = [json.start_location.lng,json.start_location.lat];
			data.end_location = [json.end_location.lng,json.end_location.lat];
			data.weight = this.$el.find('.vehicle-weight').val();

			
			$.ajax({
				url:url,
				data:JSON.stringify(data),
				type:'POST',
				contentType: "application/json; charset=utf-8",
        		async:false,
        		success:function(res){
        			if(res.ok){
        				alert('Road Added to Blacklist.');
        			}
        		},
        		error:function(err){
        			console.log(err);
        		}
			});
		},
		drawTraceLine:function(){
			Communicator.command.execute('map:drawTraceLine',this.model.get('path'))
		},
		clearTraceLine:function(){
			Communicator.command.execute('map:clearTraceLine');
		},
		onRender: function() {
			this.$el.find('.info-route-list-index').html(this.options.index+1)
		}
	});
});