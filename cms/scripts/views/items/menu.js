define([
	'backbone',
	'communicator',
	'hbs!tmpl/items/menu'
],
function( Backbone, Communicator, menu  ) {
    'use strict';
	return Backbone.Marionette.ItemView.extend({
		initialize: function() {
		},
	  	template: menu,
  		ui: {},
		events: {},
		onRender: function() {
		}
	});
});