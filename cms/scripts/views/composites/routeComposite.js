define([
	'backbone',
  'communicator',
	'views/items/route',
  'hbs!tmpl/composites/routeComposite'
],
function( Backbone, Communicator, route, routeComposite   ) {
  'use strict';
	return Backbone.Marionette.CompositeView.extend({
		initialize: function() {
		},	
  	childView: route,
  	template: routeComposite,
  	ui: {},
		events: {
    },

		childViewContainer: ".info-route-list-container",
    
    childViewOptions:function(model, index){
      return {index:index}
    },
		onRender: function() {
    }
	});
});