define([
	'backbone',
  'communicator',
	'views/items/suggestion',
  'hbs!tmpl/composites/suggestionComposite'

],
function( Backbone, Communicator, suggestion, suggestionComposite   ) {
  'use strict';
	return Backbone.Marionette.CompositeView.extend({
		initialize: function() {
		},
  		childView: suggestion,
  		template: suggestionComposite,
  		ui: {},
		events: {},
		childViewContainer: ".menu-suggestion-list-container",
		onRender: function() {
    }

 
	});
});