define([   'app', 'communicator'
], function(App ,  Communicator) {
  'use strict';
  App.module('Map', function(Map, App) {


    Map.route = [];
    Map.start_loc = {};
    Map.end_loc = {};

    Map.createMap = function(dom_id){
      Map.map = new AMap.Map(dom_id, {
        resizeEnable: true,
        center: [113.326252, 23.143697],
        zoom:15
      });
      
      Map.map.plugin(["AMap.Autocomplete"], function() {
        Map.map.autoComplete = new AMap.Autocomplete();
      });

      AMap.service(["AMap.Driving",'AMap.Geocoder'], function() {
        Map.MDrive = new AMap.Driving();
        Map.Geocoder = new AMap.Geocoder();
      });

      Map.marker = {};

      var sicon = new AMap.Icon({
        image: "http://cache.amap.com/lbs/static/jsdemo002.png",
        size:new AMap.Size(44,44),
        imageOffset: new AMap.Pixel(-334, -180)
      });

      Map.marker.start = new AMap.Marker({
        icon : sicon,
        map:Map.map,
        visible:false,
        offset : {
          x : -16,
          y : -40
        }      
      });

      var eicon = new AMap.Icon({
        image: "http://cache.amap.com/lbs/static/jsdemo002.png",
        size:new AMap.Size(44,44),
        imageOffset: new AMap.Pixel(-334, -134)
      });


      Map.marker.end = new AMap.Marker({
        icon : eicon,
        map:Map.map,
        visible:false,
        offset : {
          x : -16,
          y : -40
        }      
      });

      Map.routeLine = null; // Navigate line
      Map.traceLine = null; // Overlay line
    }
    
//Communicator Events===========================================================

    Communicator.command.setHandler('map:getAddressByLngLat',function(loc){
      Map.Geocoder.getAddress(loc,function(status,res){
        if(status === 'complete' && res.info === 'OK'){
          Communicator.mediator.trigger('map:address',res.regeocode.addressComponent);
        }else{
          alert(res);
        }
      });
    });

    Communicator.reqres.setHandler('map:autoCompleteSuggestion',function(keywords,callback){
      Map.map.autoComplete.search(keywords,function(status,res){
        callback(res.tips);
      });
    });

    Communicator.command.setHandler('map:markPoint',function(loc,point){
      Map.map.panTo(loc);
      if(Map.routeLine!= null){
        Map.routeLine.setMap(null);
      }
      Map.marker[point].show();
      Map.marker[point].setPosition(loc);
      Map.map.setFitView();
    });
    
    Communicator.command.setHandler('map:drawTraceLine',function(path){
      if(Map.traceLine != null){
        Map.traceLine.setMap(null);
      }

      Map.traceLine = new AMap.Polyline({
        map: Map.map,
        path: path,
        strokeColor: "#FF3030",
        strokeOpacity: 0.9,
        strokeWeight: 8,
        strokeDasharray: [10, 5]
      });

      Map.map.setFitView(Map.traceLine);
    });

    Communicator.command.setHandler('map:clearTraceLine',function(path){
      Map.traceLine.setMap(null);
    });

    Communicator.mediator.on('map:routes',function(routes){
      var extra_path1 = new Array();
      extra_path1.push(Map.marker.start.getPosition());
      extra_path1.push(routes.steps[0].path[0]);
      var extra_line1 = new AMap.Polyline({
        map: Map.map,
        path: extra_path1,
        strokeColor: "#9400D3",
        strokeOpacity: 0.7,
        strokeWeight: 4,
        strokeStyle: "dashed",
        strokeDasharray: [10, 5]
      });
    
      var extra_path2 = new Array();
      var path_xy = routes.steps[(routes.steps.length-1)].path;
      extra_path2.push(Map.marker.end.getPosition());
      extra_path2.push(path_xy[(path_xy.length-1)]);
      var extra_line2 = new AMap.Polyline({
        map: Map.map,
        path: extra_path2,
        strokeColor: "#9400D3",
        strokeOpacity: 0.7,
        strokeWeight: 4,
        strokeStyle: "dashed",
        strokeDasharray: [10, 5]
      });
      
      var drawpath = new Array(); 
      for(var s=0; s<routes.steps.length; s++) {
        var plength = routes.steps[s].path.length;
        for (var p=0; p<plength; p++) {
          drawpath.push(routes.steps[s].path[p]);
        }
      }

      if(Map.routeLine != null){
        Map.routeLine.setMap(null);
      }
      Map.routeLine = new AMap.Polyline({
        map: Map.map,
        path: drawpath,
        strokeColor: "#9400D3",
        strokeOpacity: 0.7,
        strokeWeight: 4,
        strokeDasharray: [10, 5]
      });
      Map.map.setFitView(Map.routeLine);
    });

    Communicator.command.setHandler('map:navigation',function(start,end,weight){
      if(!Map.naviLock){
        Map.route = [];
        Map.start_loc = start;
        Map.end_loc = end;
        Map.naviLock = true; // lock further navigation request till current one finished
        navigate(Map.start_loc,Map.end_loc);
      }
    });

    Communicator.mediator.on('map:doneOp',function(){
      Map.naviLock = false;
    });

    Communicator.command.setHandler('map:resetZoom',function(){
      if(Map.routeLine != null){
        Map.map.setFitView(Map.routeLine);
      }
    });

//Private functions============================================= 

    function navigate(s,e,avoid){

      var start = new AMap.LngLat(s.lng,s.lat);
      var end   = new AMap.LngLat(e.lng,e.lat);   
      if(avoid != undefined && avoid != null){
        Map.MDrive.setAvoidRoad(avoid);
      }else{
        Map.MDrive.clearAvoidRoad();
      }
      
      Map.MDrive.search(start, end, function(status, res){
        if(status === 'complete' && res.info === 'OK'){
          if(res.routes.length <= 0){
            alert('天杀的...搞毛啊？');
            Communicator.mediator.trigger('map:doneOp');
          }else{
            Map.route = res.routes[0].steps;
            Communicator.mediator.trigger('map:routes',createAMapRoute());
            Communicator.mediator.trigger('map:doneOp');
          }
        }else{
          alert(res);
        }
      });
    }
    
    function createAMapRoute(){
      var route = {};
      route.steps = Map.route;
      route.time = 0;
      route.tolls = 0;
      route.distance = 0;
      for(var i in route.steps){
        route.time+= route.steps[i].time;
        route.distance+= route.steps[i].distance;
        route.tolls+= route.steps[i].tolls;
      }
      return route;
    }
    
    return App.module('Map');
  });

});

