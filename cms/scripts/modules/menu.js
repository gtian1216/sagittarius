define([
    'app',
    'communicator',
    'views/layouts/menu',
    'views/composites/suggestionComposite',
    'collections/suggestionCollection',
    'views/composites/routeComposite',
    'collections/routeCollection',


], function(App, Communicator, menu_l,suggestion_c, suggestionCollection, route_c, routeCollection) {

  'use strict';

    App.module('Menu', function(Menu, App) {
        Menu.layout = new menu_l();
        Menu.suggestionCollection = new suggestionCollection();
        Menu.suggestionComp = new suggestion_c({collection:Menu.suggestionCollection});
        Menu.routeCollection = new routeCollection();
        Menu.routeComp = new route_c({collection:Menu.routeCollection});


        Menu.display = function(region) {  
          region.show(Menu.layout); 
          Menu.layout.suggestion.show(Menu.suggestionComp);
          Menu.layout.route.show(Menu.routeComp);
          App.Map.createMap('mapContainer');
        }
        
        Communicator.mediator.on('map:suggestion',function(data){
          if(data && !Communicator.reqres.request('menu:pauseSuggestion')){
            Menu.suggestionCollection.set(data);
          }else{
            Menu.suggestionCollection.reset();
          }
        });

        Communicator.mediator.on('map:routes',function(data){
          Menu.routeCollection.set(data.steps);
        });

    });


    return App.module('Menu');
});