# Sagittarius Routing
### Server / App Deployment
- Install NodeJS ( > 4.0.0)
- Install NPM if necessary

```sh
npm install -g grunt-cli bower
```
- Run following command to deploy server
```sh
#at root directory
npm install
#at root directory
cd app
bower install
#at root directory
cd cms
bower install
```

- To run server
```sh
#at root directory 
grunt
```

- To build
```sh
#at root directory 
grunt build
#after package built, use nginx or apache to host web service point to dist folder
```

- To build gas daily price DB
```sh
#at root directory 
cd server/scripts
node updateDailyGasPrice.js
#This script should be run on a cron
```