require.config({

    baseUrl: "/scripts",

    /* starting point for application */
    deps: ['backbone.marionette','main'],


    shim: {
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        underscore:{ exports: '_'},
        bootstrap : { deps :['jquery'], exports: 'Bootstrap' }
    },
    paths: {
        'jquery': '../bower/jquery/dist/jquery',
        'backbone': '../bower/backbone/backbone',
        'bootstrap': '../bower/bootstrap/dist/js/bootstrap',
        'underscore': '../bower/underscore/underscore',
        'backbone.marionette': '../bower/backbone.marionette/lib/core/backbone.marionette',
        'backbone.wreqr': '../bower/backbone.wreqr/lib/backbone.wreqr',
        'backbone.babysitter': '../bower/backbone.babysitter/lib/backbone.babysitter',
        'hbs': '../bower/require-handlebars-plugin/hbs',
        'tmpl' : '../templates',
    },
    hbs: { // optional
        helpers: true,            // default: true
        i18n: false,              // default: false
        templateExtension: 'hbs', // default: 'hbs'
    }
});
