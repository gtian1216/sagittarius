define([
    'app',
    'views/layouts/menu',
    'views/items/suggestion',
    'views/composites/suggestionComposite',
    'collections/suggestionCollection',
    'communicator'
], function(App, menu_l, suggestion_i, suggestion_c, suggestionCollection, Communicator) {

  'use strict';

    App.module('Menu', function(Menu, App) {
        Menu.layout = new menu_l();
        Menu.suggestionCollection = new suggestionCollection();
        Menu.suggestionComp = new suggestion_c({collection:Menu.suggestionCollection});
       
        
        Menu.display = function(region) {  
          region.show(Menu.layout); 
          Menu.layout.suggestion.show(Menu.suggestionComp);
        }

        Communicator.mediator.on('map:suggestion',function(data){
          if(data && !Communicator.reqres.request('menu:pauseSuggestion')){
            Menu.suggestionCollection.set(data);
          }else{
            Menu.suggestionCollection.reset();
          }
        });

    });

    App.module('Menu.suggestion',function(Suggestion,App){});

    return App.module('Menu');
});