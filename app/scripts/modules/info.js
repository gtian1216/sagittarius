define([
    'app',
    'communicator',
    'views/layouts/info',
    'views/composites/routeComposite',
    'collections/routeCollection',
    'models/routeMeta',
    'views/items/routeMeta'

], function(App, Communicator, info_l,route_c, routeCollection,routeMeta_m, routeMeta_v) {

  'use strict';

    App.module('Info', function(Info, App) {
        Info.layout = new info_l;
        Info.routeCollection = new routeCollection();
        Info.routeComp = new route_c({collection:Info.routeCollection});
        Info.routeModel = new routeMeta_m();
        Info.routeMetaView = new routeMeta_v({model:Info.routeModel});
               
        Info.display = function(region) {  
          region.show(Info.layout); 
          Info.layout.content.show(Info.routeComp);
          Info.layout.title.show(Info.routeMetaView);
        }

        Communicator.mediator.on('map:routes',function(data){


          data.hour = Math.floor(data.time/3600);
          data.minute = Math.floor((data.time-data.hour*3600)/60);
          data.second = data.time - data.hour*3600 - data.minute*60;

          data.distanceInKM = Math.ceil(data.distance/10)/100;
          data.gasConsumption = Math.ceil(data.distanceInKM * Communicator.reqres.request('menu:getGasMileage'))/100;
          data.gasSpending = Math.ceil((data.gasConsumption * Communicator.reqres.request('menu:getDieselPrice'))*100)/100;

          Info.routeCollection.set(data.steps);
          Info.routeModel.set(data);
          
          Communicator.command.execute('info:show');
        });
    });


    return App.module('Info');
});