require([
  'backbone',
  'app',
  'region',
  'modules/amap',
  'modules/menu',
  'modules/info'
],
function (Backbone, app, region ) {
  'use strict';	
  app.start();
});
