define([
	'backbone',
	'models/suggestion'
],
function( Backbone, suggestion ) {
    'use strict';
	return Backbone.Collection.extend({
		initialize: function() {},
		model: suggestion		
	});
});