define([
	'backbone',
	'models/route'
],
function( Backbone, route ) {
    'use strict';
	return Backbone.Collection.extend({
		initialize: function() {},
		model: route		
	});
});