define([
	'backbone',
	'models/step'
],
function( Backbone, step ) {
    'use strict';
	return Backbone.Collection.extend({
		initialize: function() {},
		model: step		
	});
});