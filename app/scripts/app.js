define([
	'backbone',
	'communicator',
],

function( Backbone, Communicator) {
	'use strict';

	var App = new Backbone.Marionette.Application();

	App.addInitializer( function () {
		Communicator.reqres.request('region:add', 'menu', '#menu');
		Communicator.reqres.request('region:add', 'info', '#info');
		App.Menu.display(Communicator.reqres.request('region:get', 'menu'));
		App.Info.display(Communicator.reqres.request('region:get', 'info'));
		App.Map.createMap('mapContainer');
	});
	
	return App;
});
