define( ['backbone','communicator','hbs!tmpl/layouts/info'],
function( Backbone, Communicator,info_l) {
    'use strict';
	return Backbone.Marionette.LayoutView.extend ({
		initialize: function() {
            Communicator.command.setHandler('info:show',this.show,this);
		},
        events:{
            'click #info-toggle':'toggleContainer',
            'click #info-resize':'resizeContainer',
            'click #check':'enlarge',
            'mouseleave #info-container':'resetZoomLevel',
        },
        template: info_l,
        regions:{
            title:'#info-title',
            content:'#info-content'
        },
        resizeContainer:function(ev){
            if($(ev.target).hasClass('fa-chevron-circle-up')){
                $(ev.target).removeClass('fa-chevron-circle-up').addClass('fa-chevron-circle-down');
                this.$el.find('#info-container').css('height','700px');
            }else{
                $(ev.target).removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-up');
                this.$el.find('#info-container').css('height','400px');
            }
        },
        show:function(){
            this.$el.find('#info-container').show();
        },
        toggleContainer:function(){
            this.$el.find('#info-container').toggle();
        },
        resetZoomLevel:function(){
          Communicator.command.execute('map:resetZoom');
        },
        onRender: function() {
        }
    });
});