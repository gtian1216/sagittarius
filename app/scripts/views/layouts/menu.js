define( ['backbone','communicator','models/twopoints','hbs!tmpl/layouts/menu'],
function( Backbone, Communicator, point, menu_l) {
    'use strict';
	return Backbone.Marionette.LayoutView.extend ({
		initialize: function() {
            Communicator.reqres.setHandler('menu:pauseSuggestion',this.pauseSuggestion,this);
            Communicator.reqres.setHandler('menu:currentFocusInput',this.currentFocusInput,this);
            Communicator.reqres.setHandler('menu:getGasMileage',this.getGasMileage,this);
            Communicator.reqres.setHandler('menu:getDieselPrice',this.getDieselPrice,this);
            Communicator.command.setHandler('menu:setInput',this.setInput,this);
            Communicator.mediator.on('map:doneOp',this.hideComputing,this);
            Communicator.mediator.on('map:address',this.fetchGasPrice,this);

            this.model = new point({start_loc:{},end_loc:{},start_lock:false,end_lock:false});
		},
        events:{
        	'input .menu-address':'autoComplete',
        	'propertychange .menu-address':'autoComplete',
            'focus .menu-address':'moveSuggestion',
            'focusout .menu-address':'clearSuggestion',
            'click #navigate':'navigation',
        },
        template: menu_l,
        regions:{
            suggestion:'#menu-suggestion'
        },
        autoComplete:function(ev){
        	Communicator.reqres.request("map:autoCompleteSuggestion",$(ev.target).val(),function(data){
                Communicator.mediator.trigger('map:suggestion',data);
            });
        },
        //Move suggestion window and set current focus input field, and unlock the field for ops    
        moveSuggestion:function(ev){
            this.focusedInput = $(ev.target).attr('id');
            if(this.focusedInput == 'start')
                this.model.set({start_lock:false});
            else{
                this.model.set({end_lock:false});
            } 
            this.$el.find('#menu-suggestion').insertAfter($(ev.target).closest('div'));
        },
        clearSuggestion:function(){
            if(!this.$el.find('#menu-suggestion').is(":hover")){
                Communicator.mediator.trigger('map:suggestion',false);
            }
        },
        pauseSuggestion:function(){
             if(this.$el.find('#'+this.focusedInput).val().length <= 0  || this.model.get(this.focusedInput+'_lock')){
                return true;
             }else{
                return false
             }
        },
        currentFocusInput:function(){
            return this.focusedInput;
        },
        //Set input field to selected item from suggestion and lock the field
        setInput:function(data,point){
            this.$el.find('#'+point).val(data.name+' '+data.district);
            this.$el.find('#'+point).data(data);
            if(point == 'start'){
                this.model.set('start_loc',data.location);
                this.model.set('start_lock',true);
                Communicator.command.execute('map:getAddressByLngLat',[data.location.lng,data.location.lat]);
            }else{
                this.model.set('end_loc',data.location);
                this.model.set('end_lock',true);               
            }
        },
        navigation:function(){
            if(this.model.get('start_lock') && this.model.get('end_lock')){
                var s = this.model.get('start_loc');
                var e = this.model.get('end_loc');
                var w = this.$el.find('#vehicle-weight').val();
                this.$el.find('#menu-computing').show();
                Communicator.command.execute('map:navigation',{lng:s.lng,lat:s.lat},{lng:e.lng,lat:e.lat},w);
            }
        },
        hideComputing:function(){
            this.$el.find('#menu-computing').hide();
        },
        getDieselPrice:function(){
            return this.$el.find('#diesel-price').val();
        },
        getGasMileage:function(){
            return this.$el.find('#gas-mileage').val();
        },
        fetchGasPrice:function(address){
            var that = this;
            var url = 'api/v1/GetGasPriceByProvince';
            var data = {province: address.province};
            $.ajax({
                url:url,
                data:data,
                success:function(res){
                    that.$el.find('#diesel-price').val(res.oil0);
                },
                error:function(err){
                    console.log(err);
                }  
            });
        },
        onRender: function() {
        }
    });
});