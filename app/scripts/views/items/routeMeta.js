define([
	'backbone',
	'communicator',
	'hbs!tmpl/items/routeMeta'
],
function( Backbone, Communicator, routeMeta  ) {
    'use strict';
	return Backbone.Marionette.ItemView.extend({
		initialize: function(model) {
			this.listenTo(this.model,'change',this.render);
		},
	  	template: function(info) {
	      return routeMeta(info);
	    },
  		ui: {},
		events: {},
		render:function(){
 			this.$el.html(routeMeta(this.model.toJSON()));
    		return this;
		},
		onRender: function() {
		}
	});
});