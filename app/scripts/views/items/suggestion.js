define([
	'backbone',
	'communicator',
	'hbs!tmpl/items/suggestion'
],
function( Backbone, Communicator, suggestion  ) {
    'use strict';
	return Backbone.Marionette.ItemView.extend({
		initialize: function() {
		},
		tagName:'li',
		className:'list-group-item',
	  	template: function(tip) {
	      return suggestion(tip);
	    },
  		ui: {},
		events: {
			'click':'markMap'
		},
		markMap:function(ev){
			var data = this.model.toJSON();
			var point = Communicator.reqres.request('menu:currentFocusInput');
			Communicator.command.execute('map:markPoint',[data.location.lng,data.location.lat],point);
			Communicator.mediator.trigger('map:suggestion',false);
			Communicator.command.execute('menu:setInput',data,point);
		},
		onRender: function() {
		}
	});
});