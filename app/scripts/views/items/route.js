define([
	'backbone',
	'communicator',
	'hbs!tmpl/items/route'
],
function( Backbone, Communicator, route  ) {
    'use strict';
	return Backbone.Marionette.ItemView.extend({
		initialize: function() {
		},
		tagName:'li',
		className:'list-group-item info-route-list-item',
	  	template: function(step) {
	      return route(step);
	    },
  		ui: {},
		events: {
			'mouseover .info-route-highlight':'drawTraceLine',
			'mouseout  .info-route-highlight':'clearTraceLine'
		},
		drawTraceLine:function(){
			Communicator.command.execute('map:drawTraceLine',this.model.get('path'))
		},
		clearTraceLine:function(){
			Communicator.command.execute('map:clearTraceLine');
		},
		onRender: function() {
			this.$el.find('.info-route-list-index').html(this.options.index+1)
		}
	});
});